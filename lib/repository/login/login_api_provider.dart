import 'package:dio/dio.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/util/AlertMessages.dart';
import 'package:flutter_revision/util/ApiHelper.dart';

class LoginApiProvider {
  Future<LoginResponse> loginUser(String mobileNumber, String password) async {
    Map<String, String> body = {
      "email": mobileNumber,
      "password": password,
    };

    try {
      Response response = await ApiHelper.getDioClient().post("login", data: body);
      print(response.toString());
      return LoginResponse(
        code: 200,
        message: "Success",
        token: "Test Token 123",
        data: UserData(
          userId: 123,
          mobileNumber: "8896783489",
          fullName: "Test Login user",
        ),
      );
    } catch (e) {
      print(e);
      if (e is DioError) {
        return LoginResponse(code: 101, message: ApiHelper.getErrorMsg(e), data: null);
      } else {
        return LoginResponse(code: 101, message: AlertMessages.GENERIC_ERROR_MSG, data: null);
      }
    }
  }

/*
dynamic _handleResponse<T>(Response response, T responseType) T as Class {
    T data;
data =responseType;

    switch (response.statusCode) {
      case 200:
      //  var responseJson = json.decode(response.data.toString());
       */ /* ApiResponse<responseType>(
            statusCode: loginUserModel.code,
            message: loginUserModel.message,
            data: loginUserModel);*/ /*
        return ApiResponse<data>.fromJson(response.data);


        print(responseJson);
        return responseJson;

        return ApiResponse(
            statusCode: response.statusCode,
            message: "error",
            data: responseType.fromJson(json.decode(response.data)));

      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }*/

}

/*class ServerResponse<T> {
  int code;
  String message;
  T  data;

  ServerResponse({
    this.code,
    this.message,
    this.data,
  });

  ServerResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    //data = LoginUserModel.fromJson(json['data']);
    data = LoginUserModel.fromJson(json['data']) as T;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    data['data'] = this.data;
    return data;
  }
}*/
