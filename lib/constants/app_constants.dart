import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppConstants {
  static ThemeData themeData = ThemeData(
    brightness: Brightness.light,
    primaryColor: AppColors.primaryColor,
    accentColor: AppColors.accentColor,
    textTheme: TextTheme(
      headline1: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Color(0xff5C5C5C)),
      headline2: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal, color: Color(0xff9F9F9F)),
      headline3: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal, color: Color(0xff52A811)),
      bodyText1: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal, color: Color(0xffACACAC)),
    ),
  );
}
