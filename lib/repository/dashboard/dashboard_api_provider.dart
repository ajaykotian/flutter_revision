import 'package:dio/dio.dart';
import 'package:flutter_revision/model/dashboard_response.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/util/ApiHelper.dart';
import 'package:flutter_revision/util/AppUtils.dart';

class DashboardApiProvider {
  Future<DashboardResponse> getDashboardData(String userDealerRelId) async {
    Map<String, String> body = {"userDealerRelId": userDealerRelId};
    print(body.toString());
    try {
      String token = await AppUtils().getUserToken();
      print(token.toString());
      Response response = await ApiHelper.getDioClient().post("users/2",
          data: body,
          options: Options(
            headers: {"Authorization": "Bearer " + token},
          ));
      print(response.toString());
      return DashboardResponse(
        code: 200,
        message: "Successs",
        data: UserData(
          userId: 123,
          mobileNumber: "9892809689",
          fullName: "Full Name",
        ),
      ); //DashboardResponse.fromJson(response.data);
    } on DioError catch (e) {
      print(e);
      return DashboardResponse(code: 101, message: ApiHelper.getErrorMsg(e), data: null);
    }
  }
}
