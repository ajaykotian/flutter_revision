import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_revision/bloc/dashboard/dashboard_bloc.dart';
import 'package:flutter_revision/bloc/dashboard/dashboard_events.dart';
import 'package:flutter_revision/bloc/dashboard/dashboard_states.dart';
import 'package:flutter_revision/bloc/user/user_bloc.dart';
import 'package:flutter_revision/constants/app_colors.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/util/AlertUtil.dart';
import 'package:flutter_revision/util/AppUtils.dart';
import 'package:flutter_revision/util/Loader.dart';
import 'package:flutter_revision/util/const.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  final Function(int) onTabSelected;
  final Function(bool) onscreenLoaded;

  //DashboardScreen(this.callback, { Key key }) : super(key: key);

  DashboardScreen({required this.onTabSelected, required this.onscreenLoaded});

  // final TabCallback onTabChange;

  @override
  DashboardScreenState createState() {
    return new DashboardScreenState(key);
  }
}

class DashboardScreenState extends State<DashboardScreen> {
  GlobalKey<ScaffoldState>? customWidgetKey;

  DashboardScreenState([Key? key]);

  final dashboardBloc = DashboardBloc();
  UserData? dashboardData;
  late UserBloc userBloc;

  final globalKey = GlobalKey<ScaffoldState>();
  String _userName = "";
  String _accountName = "";
  String _accountId = "";
  String _userDealerRelId = "";

  @override
  void initState() {
    super.initState();

    AppUtils().getUserName().then((String userName) {
      AppUtils().getUserCurrentAccount().then((String accountName) {
        AppUtils().getUserCurrentAccountId().then((String accountId) {
          setState(() {
            _userName = userName;
            _accountName = accountName;
            _accountId = accountId;
          });
        });
      });
    });

    AppUtils().getUserDealerRelId().then((String userDealerRelId) {
      /*setState(() {
        _accountId = userDealerRelId;
      });*/
      _userDealerRelId = userDealerRelId;
      AppUtils().getUserToken().then((String token) {
        Constants.userToken = token;
        dashboardBloc.add(GetDashboardData(userDealerRelId: userDealerRelId));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    userBloc = Provider.of<UserBloc>(context);
    return Scaffold(
      key: globalKey,
      body: SafeArea(
        child: BlocProvider(
          create: (BuildContext context) => dashboardBloc,
          child: BlocListener(
            bloc: dashboardBloc,
            listener: (BuildContext context, DashboardState state) {
              if (state is LoadingState) {
                Loader.show(context);
              } else if (state is SuccessState) {
                widget.onscreenLoaded(true);
                Loader.hide();
                setState(() {
                  dashboardData = state.dashboardData;
                  /* List<ProductDetails> productDetailsNew = [];
                  productDetailsNew.add(dashboardData.myStock.productDetails[0]);
                  productDetailsNew.add(dashboardData.myStock.productDetails[1]);
                  dashboardData.myStock.productDetails = productDetailsNew;*/
                });
              } else if (state is ErrorState) {
                widget.onscreenLoaded(true);
                Loader.hide();
                AlertUtil.showAlertPopup(context, "Alert", state.errorMessage);
              }
            },
            child: SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: AppColors.screenBgColor,
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        //Header
                        Container(
                          padding: EdgeInsets.only(left: 0, top: 11, right: 0),
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Container(
                                  color: Colors.transparent,
                                  margin: EdgeInsets.only(left: 20, top: 5),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            "Hello",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 16, color: AppColors.primaryColor, fontFamily: 'poppins_regular'),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 5),
                                            child: Text(
                                              userBloc.userData?.fullName.split(" ")[0] ?? "-" + "!",
                                              textAlign: TextAlign.start,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: 20, color: AppColors.primaryColor, fontFamily: 'poppins_medium'),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        "Your Mobile Number is",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 16, color: AppColors.primaryColor, fontFamily: 'poppins_regular'),
                                      ),
                                      Container(
                                        child: Text(
                                          userBloc.userData?.mobileNumber ?? "-" + "!",
                                          textAlign: TextAlign.start,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(fontSize: 20, color: AppColors.primaryColor, fontFamily: 'poppins_medium'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
