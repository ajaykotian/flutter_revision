import 'package:dio/dio.dart';

import 'AlertMessages.dart';
import 'const.dart';

class ApiHelper {
  static Map<String, dynamic> customHeaders = {'content-type': 'application/json'};

  static final BaseOptions options = new BaseOptions(
    baseUrl: Constants.BASE_URL,
    connectTimeout: 40000,
    receiveTimeout: 60000,
    headers: customHeaders,
    followRedirects: false,
    validateStatus: (status) {
      return status != null && status < 500;
    },
  );

  static getDioClient() {
    Dio dio = Dio(options);
    return dio;
  }

  static getErrorMsg(DioError e) {
    if (DioErrorType.receiveTimeout == e.type || DioErrorType.connectTimeout == e.type) {
      return AlertMessages.TIMEOUT_ERROR_MSG;
    } else if (DioErrorType.response == e.type) {
      return AlertMessages.GENERIC_ERROR_MSG;
    } else if (DioErrorType.other == e.type) {
      if (e.message.contains('SocketException')) {
        return AlertMessages.OFFLINE_ERROR_MSG;
      }
    } else {
      return AlertMessages.GENERIC_ERROR_MSG;
    }
  }

  /// Login with Email API call
/* static Future<LoginResponse> loginWithEmail(
      String email, String password) async {
    Map<String, dynamic> body = {
      "operation": "login",
      "email": email,
      "password": password
    };

    FormData formData = new FormData.fromMap(body);
    try {
      Response response = await dio.post("/index.php", data: formData);
      if (response.statusCode == 200)
        return LoginResponse.fromJson(json.decode(response.data));
      else
        throw Exception('Failed to get data');
    } catch (e) {
      print(e);
      throw Exception('Failed to get data');
    }
  }

  /// SignUp with email API call
  static Future<SignUpResponse> signUpWithEmail(
      String username, String email, String password) async {
    Map<String, dynamic> body = {
      "operation": "signup",
      "user_name": username,
      "email": email,
      "password": password,
      "account_type": "email"
    };

    FormData formData = new FormData.fromMap(body);
    try {
      Response response = await dio.post("/index.php", data: formData);
      if (response.statusCode == 200)
        return SignUpResponse.fromJson(json.decode(response.data));
      else
        throw Exception('Failed to get data');
    } catch (e) {
      print(e);
      throw Exception('Failed to get data');
    }
  }*/
}
