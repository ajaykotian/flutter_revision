import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  static OverlayEntry? currentLoader;
  static late String title;

  Loader(String s) {
    title = s;
  }

  static void show(BuildContext context) {
    title = "";
    OverlayEntry tempCurrentLoader = new OverlayEntry(
        builder: (context) => Stack(
              children: <Widget>[
                Container(
                  color: Color(0x80000000),
                ),
                Center(
                  child: Loader(""),
                ),
              ],
            ));
    currentLoader = tempCurrentLoader;
    Overlay.of(context)?.insert(tempCurrentLoader);
  }

  static void showWithTitle(BuildContext context, String title) {
    OverlayEntry tempCurrentLoader = new OverlayEntry(
        builder: (context) => Stack(
              children: <Widget>[
                Container(
                  color: Color(0x80000000),
                ),
                Center(
                  child: Loader(title),
                ),
              ],
            ));
    Overlay.of(context)?.insert(tempCurrentLoader);
  }

  static void hide() {
    currentLoader?.remove();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FittedBox(
        child: Container(
          padding: EdgeInsets.all(15),
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 25,
                height: 25,
                child: CircularProgressIndicator(strokeWidth: 1.8),
              ),
              title.isNotEmpty
                  ? Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(
                        title,
                        style: TextStyle(decoration: TextDecoration.none, fontSize: 13, fontFamily: "poppins_medium", color: const Color(0xff5C5C5C)),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
