import 'package:flutter/cupertino.dart';
import 'package:flutter_revision/model/login_response.dart';

class UserBloc with ChangeNotifier {
  late UserData? _userData;
  String _selectedAccount = "";
  String _selectedAccountId = "";
  String _selectedAccountType = "";
  String _selectedUserDealerRelId = "";
  String _selectedAccountAddress = "";
  String _selectedAccountAgency = "";
  bool _isDashboardTutorialCompleted = false;
  bool _isMyStockTutorialCompleted = false;

  UserData? get userData => _userData;

  String get selectedAccount => _selectedAccount;

  String get selectedAccountId => _selectedAccountId;

  String get selectedAccountType => _selectedAccountType;

  String get selectedUserDealerRelId => _selectedUserDealerRelId;

  String get selectedAccountAddress => _selectedAccountAddress;

  String get selectedAccountAgency => _selectedAccountAgency;

  bool get isDashboardTutorialCompleted => _isDashboardTutorialCompleted;

  bool get isMyStockTutorialCompleted => _isMyStockTutorialCompleted;

  void setDashboardTutorialCompleted(bool isDashboardTutorialCompleted) {
    _isDashboardTutorialCompleted = isDashboardTutorialCompleted;
  }

  void setMyStockTutorialCompleted(bool isMyStockTutorialCompleted) {
    _isMyStockTutorialCompleted = isMyStockTutorialCompleted;
  }

  void setSelectedAccountAgency(String agency) {
    _selectedAccountAgency = agency;
  }

  void setSelectedAccountAddress(String address) {
    _selectedAccountAddress = address;
  }

  void setUserData(UserData userData) {
    _userData = userData;
    notifyListeners();
  }

  void clear(index) {
    notifyListeners();
  }
}
