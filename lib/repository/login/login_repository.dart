import 'package:flutter_revision/model/login_response.dart';
import 'login_api_provider.dart';

class LoginRepository {
  LoginApiProvider _loginApiProvider = LoginApiProvider();

  Future<LoginResponse> loginUser(String mobileNumber, String password) => _loginApiProvider.loginUser(mobileNumber, password);
}
