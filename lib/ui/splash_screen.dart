import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_revision/bloc/user/user_bloc.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/util/AppUtils.dart';
import 'package:flutter_revision/util/Routes.dart';
import 'package:flutter_revision/util/size_config.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  bool _isLoggedIn = false;

  late AnimationController animationController;
  late Animation<double> animation;

  startTime() async {
    var _duration = new Duration(milliseconds: 4000);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    AppUtils().getUserLoggedIn().then((bool isLoggedIn) {
      _isLoggedIn = isLoggedIn;
      if (_isLoggedIn) {
        Navigator.pushReplacementNamed(context, Routes.homeRoute);
      } else {
        AppUtils().isWalkthroughCompleted().then((bool isWalkthroughCompleted) {
          //if (isWalkthroughCompleted) {
          Navigator.pushReplacementNamed(context, Routes.loginRoute);
          /*} else {
            Navigator.pushReplacementNamed(context, '/walkthrough');
          }*/
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    startTime();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    )..addListener(() => setState(() {}));
    animation = CurvedAnimation(
      parent: animationController,
      curve: Curves.easeInOut,
    );

    animationController.forward();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var userBloc = Provider.of<UserBloc>(context);
    AppUtils().getUserData().then((String userDataStr) {
      UserData userData = UserData.fromJson(json.decode(userDataStr));
      userBloc.setUserData(userData);
      AppUtils().isDashboardTutorialCompleted().then((bool isDashboardTutorialCompleted) {
        userBloc.setDashboardTutorialCompleted(isDashboardTutorialCompleted);
      });
      AppUtils().isMyStockTutorialCompleted().then((bool isMyStockTutorialCompleted) {
        userBloc.setMyStockTutorialCompleted(isMyStockTutorialCompleted);
      });
    });

    return Scaffold(
      body: new Center(
        child: Container(
          color: Color(0xffE8F8DC),
          width: MediaQuery.of(context).size.width,
          child: new Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            // App Logo on screen center top
            ScaleTransition(
              scale: animation,
              child: Container(
                margin: EdgeInsets.only(bottom: 10.0),
                width: 280.0,
                height: 280.0,
                decoration: new BoxDecoration(
                  image: DecorationImage(
                    image: new ExactAssetImage('assets/icons/launcher.png'),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
