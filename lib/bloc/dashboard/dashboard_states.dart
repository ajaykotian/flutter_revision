import 'package:flutter_revision/model/login_response.dart';

abstract class DashboardState {}

class InitialState extends DashboardState {}

class LoadingState extends DashboardState {}

class SuccessState extends DashboardState {
  final UserData dashboardData;

  SuccessState({required this.dashboardData});
}

class ErrorState extends DashboardState {
  final String errorMessage;

  ErrorState({required this.errorMessage});
}
