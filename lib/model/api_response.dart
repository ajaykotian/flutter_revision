class ApiResponse<T> {
  int? statusCode;
  String? message;
  T? data;

  ApiResponse({
    this.statusCode,
    this.message,
    this.data,
  });

  ApiResponse.fromJson(Map<String, dynamic> json) {
    statusCode = json['code'];
    message = json['message'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.statusCode;
    data['message'] = this.message;
    data['data'] = this.data;
    return data;
  }

  @override
  String toString() {
    return "Status : $statusCode \n Message : $message \n Data : $data";
  }
}
