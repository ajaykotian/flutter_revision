class Constants {
  static String BASE_URL = "https://reqres.in/api/";

  static String appName = "Flutter Revision";

  static String userToken = "";

  static String userType = "NORMAL_USER";
}
