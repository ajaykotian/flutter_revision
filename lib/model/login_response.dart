class LoginResponse {
  int? code;
  String? message;
  String? token;
  UserData? data;

  LoginResponse({
    this.code,
    this.message,
    this.token,
    this.data,
  });

  LoginResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    token = json['token'];
    data = json['data'] != null ? UserData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    data['token'] = this.token;
    data['data'] = this.data;
    return data;
  }
}

class UserData {
  int userId;
  String mobileNumber;
  String fullName;
  bool? active;
  bool? isInvitedUser;
  String? token;
  bool? invitedUser;

  UserData({required this.userId, required this.mobileNumber, required this.fullName, this.active, this.isInvitedUser, this.token, this.invitedUser});

  factory UserData.fromJson(Map<String, dynamic> json) {
    return UserData(userId: json['userId'] as int, mobileNumber: json['mobileNumber'] as String, fullName: json['fullName'] as String, active: json['active'], isInvitedUser: json['isInvitedUser'], token: json['token'], invitedUser: json['invitedUser']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['mobileNumber'] = this.mobileNumber;
    data['fullName'] = this.fullName;
    data['active'] = this.active;
    data['isInvitedUser'] = this.isInvitedUser;
    data['token'] = this.token;
    data['invitedUser'] = this.invitedUser;
    return data;
  }
}
