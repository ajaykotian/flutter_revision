import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OthersScreen extends StatelessWidget {
  const OthersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Others"),
        ),
        body: ListView.builder(
          itemCount: 100,
          itemBuilder: (BuildContext context, int index) {
            return Text(index.toString());
          },
        ),
      ),
    );
  }
}
