
class Routes {
  static const String homeRoute = '/home';
  static const String loginRoute = '/login';
  static const String registerRoute = '/register';
  static const String inviteRoute = '/invite';
  static const String inviteSelectAccountRoute = '/invite_select_account';
  static const String inviteSuccess = '/invite_success';
}
