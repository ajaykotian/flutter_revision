import 'package:shared_preferences/shared_preferences.dart';

class AppUtils {
  Future<bool> getUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool('user_logged_in') ?? false;
    return isLoggedIn;
  }

  void setUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_logged_in', true);
  }

  Future<bool> isWalkthroughCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isWalkthroughCompleted = prefs.getBool('is_walkthrough_completed') ?? false;
    return isWalkthroughCompleted;
  }

  void setWalkthroughCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_walkthrough_completed', true);
  }

  Future<bool> isDashboardTutorialCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDashboardTutorialCompleted = prefs.getBool('is_dashboard_tutorial_completed') ?? false;
    return isDashboardTutorialCompleted;
  }

  void setDashboardTutorialCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_dashboard_tutorial_completed', true);
  }

  Future<bool> isMyStockTutorialCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isMyStockTutorialCompleted = prefs.getBool('is_my_stock_tutorial_completed') ?? false;
    return isMyStockTutorialCompleted;
  }

  void setMyStockTutorialCompleted() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_my_stock_tutorial_completed', true);
  }

  void logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_logged_in', false);
  }

  void setUserId(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_id', userId);
  }

  Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString('user_id') ?? "";
    return userId;
  }

  void saveUserName(String userName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_name', userName);
  }

  Future<String> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userName = prefs.getString('user_name') ?? "";
    return userName;
  }

  void saveUserPhoneNumber(String phoneNumber) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_phone_number', phoneNumber);
  }

  Future<String> getUserPhoneNumber() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userPhoneNumber = prefs.getString('user_phone_number') ?? "";
    return userPhoneNumber;
  }

  void saveUserToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_token', token);
  }

  Future<String> getUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('user_token') ?? "";
    return token;
  }

  void setIsInvitedUser(bool isInvited) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_invited_user', isInvited);
  }

  Future<bool> getIsInvitedUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isInvited = prefs.getBool('is_invited_user') ?? false;
    return isInvited;
  }

  void setUserDealerRelId(String userDealerRelId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_userDealerRelId', userDealerRelId);
  }

  Future<String> getUserDealerRelId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userDealerRelId = prefs.getString('user_userDealerRelId') ?? "";
    return userDealerRelId;
  }

  void setCurrentAccountAddress(String address) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('current_account_address', address);
  }

  Future<String> getCurrentAccountAddress() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String address = prefs.getString('current_account_address') ?? "";
    return address;
  }

  void setUserCurrentAccount(String userCurrAccount) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_current_account', userCurrAccount);
  }

  Future<String> getUserCurrentAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userCurrAccount = prefs.getString('user_current_account') ?? "";
    return userCurrAccount;
  }

  void setUserCurrentAccountId(String userCurrAccount) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_current_account_id', userCurrAccount);
  }

  Future<String> getUserCurrentAccountId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userCurrAccountId = prefs.getString('user_current_account_id') ?? "";
    return userCurrAccountId;
  }

  void setUserData(String userData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_data', userData);
  }

  Future<String> getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userData = prefs.getString('user_data') ?? "";
    return userData;
  }
}
