import 'package:flutter/material.dart';
import 'package:flutter_revision/model/login_response.dart';

abstract class LoginUserState {}

class InitialState extends LoginUserState {}

class LoadingState extends LoginUserState {}

class UserLoggedInState extends LoginUserState {
  final UserData userData;

  UserLoggedInState({required this.userData});
}

class ErrorState extends LoginUserState {
  final String errorMessage;

  ErrorState({required this.errorMessage});
}
