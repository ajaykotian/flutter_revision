import 'package:flutter/material.dart';

abstract class LoginUserEvent {}

class LoginUser extends LoginUserEvent {
  final String mobileNumber;
  final String password;
  LoginUser({required this.mobileNumber, required this.password});
}
