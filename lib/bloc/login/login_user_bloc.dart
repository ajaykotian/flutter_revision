import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_revision/repository/login/login_repository.dart';
import 'package:flutter_revision/util/AlertMessages.dart';
import 'package:flutter_revision/util/const.dart';

import 'login_user_events.dart';
import 'login_user_states.dart';

class LoginUserBloc extends Bloc<LoginUserEvent, LoginUserState> {
  LoginRepository? loginRepository;

  LoginUserBloc() : super(InitialState());

  @override
  Stream<LoginUserState> mapEventToState(LoginUserEvent event) async* {
    loginRepository = LoginRepository();
    try {
      if (event is LoginUser) {
        // Outputting a state from the asynchronous generator
        yield LoadingState();
        final apiResponse = await loginRepository?.loginUser(event.mobileNumber, event.password);
        final data = apiResponse?.data;
        if (apiResponse?.code == 200 && data != null) {
          yield UserLoggedInState(userData: data);
        } else if (apiResponse?.code == 401) {
          yield ErrorState(errorMessage: AlertMessages.WRONG_LOGIN_CREDENTIALS);
        } else {
          String? message = apiResponse?.message;
          String errorMessage = AlertMessages.GENERIC_ERROR_MSG;
          if (message != null) {
            errorMessage = message;
          }
          yield ErrorState(errorMessage: errorMessage);
        }
      }
    } catch (_) {
      yield ErrorState(errorMessage: AlertMessages.GENERIC_ERROR_MSG);
    }
  }
}
