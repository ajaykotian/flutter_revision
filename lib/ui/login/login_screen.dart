import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_revision/bloc/login/login_user_bloc.dart';
import 'package:flutter_revision/bloc/login/login_user_events.dart';
import 'package:flutter_revision/bloc/login/login_user_states.dart';
import 'package:flutter_revision/bloc/user/user_bloc.dart';
import 'package:flutter_revision/constants/app_colors.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/util/AlertUtil.dart';
import 'package:flutter_revision/util/AppUtils.dart';
import 'package:flutter_revision/util/Loader.dart';
import 'package:flutter_revision/util/Routes.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  final loginUserBloc = LoginUserBloc();

  FocusNode phoneNumberFieldFocusNode = new FocusNode();
  FocusNode passwordFieldFocusNode = new FocusNode();
  TextEditingController phoneNumberController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  final globalKey = GlobalKey<ScaffoldState>();

  bool _isLoading = false;
  bool passwordVisible = true;

  bool isPhoneNumberEmpty = false;
  bool isPasswordEmpty = false;

  bool validateForm(BuildContext context) {
    String patttern = r'(^[6-9]{1}[0-9]{9}$)';
    RegExp regExp = new RegExp(patttern);

    bool status = false;
    if (phoneNumberController.text.isEmpty) {
      AlertUtil.showAlertPopup(context, "Alert", "Please enter mobile number");
      status = false;
    } else if (!regExp.hasMatch(phoneNumberController.text)) {
      AlertUtil.showAlertPopup(context, "Alert", "Please enter a valid mobile number");
      status = false;
    } else if (passwordController.text.isEmpty) {
      AlertUtil.showAlertPopup(context, "Alert", "Please enter password");
      status = false;
    } else
      status = true;

    return status;
  }

  void fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext buildContext) {
    var userBloc = Provider.of<UserBloc>(buildContext);

    return Scaffold(
      key: globalKey,
      body: SafeArea(
        /*child: ProgressDialog(*/
        child: SingleChildScrollView(
          child: BlocProvider(
            create: (BuildContext context) => loginUserBloc,
            child: BlocListener(
              bloc: loginUserBloc,
              listener: (BuildContext context, LoginUserState state) {
                if (state is LoadingState) {
                  Loader.show(buildContext);
                } else if (state is UserLoggedInState) {
                  FocusScope.of(context).requestFocus(FocusNode());
                  AppUtils().setUserLoggedIn();
                  AppUtils().setUserId(state.userData.userId.toString());
                  AppUtils().saveUserName(state.userData.fullName);
                  AppUtils().saveUserPhoneNumber(state.userData.mobileNumber);

                  // Save user data in Shared Prefs and Set in User Bloc for use in all over app screens
                  UserData userData = state.userData;
                  String userDataSt = json.encode(userData).toString();
                  AppUtils().setUserData(userDataSt);
                  userBloc.setUserData(userData);

                  phoneNumberController.text = "";
                  passwordController.text = "";
                  Loader.hide();
                  Navigator.pushReplacementNamed(context, Routes.homeRoute);
                } else if (state is ErrorState) {
                  Loader.hide();
                  AlertUtil.showAlertPopup(buildContext, "Login failed", state.errorMessage);
                }
              },
              child: Container(
                color: AppColors.screenBgColor,
                child: IgnorePointer(
                  ignoring: _isLoading,
                  child: new Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 39),
                            child: Text(
                              'LOGIN',
                              style: TextStyle(fontSize: 24, letterSpacing: 0.72, fontFamily: 'poppins_bold', color: const Color(0xff5C5C5C)),
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 2),
                            child: Text(
                              'Enter your details below to continue',
                              style: TextStyle(fontSize: 14, fontFamily: 'poppins_regular', color: const Color(0xffACACAC)),
                            ),
                          ),
                        ),

                        //Mobile Number TextField
                        Container(
                          margin: EdgeInsets.only(top: 41, left: 30, right: 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 2),
                                child: Text(
                                  'Mobile Number',
                                  style: TextStyle(fontSize: 14, fontFamily: 'poppins_regular', color: const Color(0xff9F9F9F)),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(top: 6),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(boxShadow: [
                                  BoxShadow(
                                    color: phoneNumberFieldFocusNode.hasFocus ? Color(0xff2952A811) : Colors.transparent,
                                    blurRadius: 6.0,
                                    offset: Offset(1, 3),
                                  ),
                                ]),
                                child: TextFormField(
                                  autofocus: false,
                                  inputFormatters: [
                                    new WhitelistingTextInputFormatter(RegExp("[0-9]")),
                                  ],
                                  keyboardType: TextInputType.phone,
                                  onChanged: (value) =>
                                      //updateButtonState(),
                                      null,
                                  controller: phoneNumberController,
                                  onFieldSubmitted: (String value) {
                                    /*FocusScope.of(context)
                                          .requestFocus(passwordFieldFocusNode);
                                      phoneNumberFieldFocusNode.unfocus();*/
                                    fieldFocusChange(context, phoneNumberFieldFocusNode, passwordFieldFocusNode);
                                  },
                                  style: TextStyle(fontSize: 18, fontFamily: 'poppins_medium', color: const Color(0xff3E6421)),
                                  obscureText: false,
                                  cursorColor: Color(0xff26480B),
                                  focusNode: phoneNumberFieldFocusNode,
                                  textInputAction: TextInputAction.next,
                                  textAlign: TextAlign.left,
                                  maxLength: 10,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    contentPadding: const EdgeInsets.symmetric(vertical: 17.0, horizontal: 16),
                                    border: InputBorder.none,
                                    hintText: 'Mobile Number',
                                    hintStyle: TextStyle(fontSize: 18, fontFamily: 'poppins_regular', fontStyle: FontStyle.italic, color: const Color(0xffC6C7C5)),
                                    filled: true,
                                    fillColor: phoneNumberController.text.isEmpty ? Color(0xffF9F9F9) : Color(0xffF0F8EA),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xff52A811), width: 1),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.transparent, width: 1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                        //Password TextField
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 30, right: 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 2),
                                child: Text(
                                  'Password',
                                  style: TextStyle(fontSize: 14, fontFamily: 'poppins_regular', color: const Color(0xff9F9F9F)),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(top: 6),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(boxShadow: [
                                  BoxShadow(
                                    color: passwordFieldFocusNode.hasFocus ? Color(0xff2952A811) : Colors.transparent,
                                    blurRadius: 6.0,
                                    offset: Offset(1, 3),
                                  ),
                                ]),
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Expanded(
                                      child: Stack(
                                        children: <Widget>[
                                          TextFormField(
                                            // validator: (value) => value.isEmpty ? 'Email cannot be blank':null,
                                            /* validator: (value) {
                                      // add your custom validation here.
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      if (value.length < 3) {
                                        return 'Must be more than 2 charater';
                                      } else
                                        return "";
                                    },*/
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(fontSize: 18, fontFamily: 'poppins_regular', color: const Color(0xff26480B)),
                                            onChanged: (value) =>
                                                //updateButtonState(),
                                                null,
                                            controller: passwordController,
                                            maxLength: 16,
                                            onFieldSubmitted: (String value) {
                                              FocusScope.of(context).requestFocus();
                                              passwordFieldFocusNode.unfocus();
                                            },
                                            cursorColor: Color(0xff26480B),
                                            obscureText: passwordVisible,
                                            focusNode: passwordFieldFocusNode,
                                            textInputAction: TextInputAction.done,
                                            decoration: InputDecoration(
                                              counterText: "",
                                              contentPadding: const EdgeInsets.symmetric(vertical: 17.0, horizontal: 15),
                                              border: InputBorder.none,
                                              hintText: 'Password',
                                              hintStyle: TextStyle(fontSize: 18, fontFamily: 'poppins_regular', fontStyle: FontStyle.italic, color: const Color(0xffC6C7C5)),
                                              filled: true,
                                              fillColor: passwordController.text.isEmpty ? Color(0xffF9F9F9) : Color(0xffF0F8EA),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: Color(0xff52A811), width: 1),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: Colors.transparent, width: 1),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                passwordVisible = !passwordVisible;
                                              });
                                            },
                                            child: Align(
                                              alignment: Alignment.centerRight,
                                              child: Container(
                                                color: Colors.transparent,
                                                margin: EdgeInsets.only(right: 7.82, top: 3),
                                                child: IconButton(
                                                  iconSize: 25,
                                                  autofocus: false,
                                                  padding: EdgeInsets.all(10),
                                                  icon: passwordVisible == true ? Image.asset('assets/icons/ic_eye.png') : Image.asset('assets/icons/ic_eye_green.png'),
                                                  onPressed: () {},
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ), //Password TextField

                        // Forgot Password button
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10, bottom: 10, top: 20, right: 30),
                              child: GestureDetector(
                                child: Text(
                                  "Forgot Password?",
                                  style: TextStyle(decoration: TextDecoration.underline, fontSize: 14.0, fontFamily: 'poppins_regular', color: Color(0xff52A811)),
                                  textAlign: TextAlign.end,
                                ),
                                onTap: () => {
                                  phoneNumberController.text = "",
                                  passwordController.text = "",
                                  Navigator.pushNamed(context, '/forgot_password'),
                                },
                              ),
                            ),
                          ],
                        ),

                        // Login Button
                        new Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
                          alignment: Alignment.center,
                          child: RaisedButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                            ),
                            color: Color(0xff52A811),
                            onPressed: () => {
                              FocusScope.of(context).requestFocus(FocusNode()),
                              if (validateForm(context))
                                {
                                  loginUserBloc.add(
                                    LoginUser(
                                      mobileNumber: phoneNumberController.text.toString(),
                                      password: passwordController.text.toString(),
                                    ),
                                  ),
                                },
                              /* Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                    builder: (context) => OthersScreen(),
                                  )),*/
                            },
                            child: new Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 16.0,
                                horizontal: 15.0,
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Expanded(
                                    child: Text(
                                      "Login",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'poppins_regular',
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                        // Sign Up
                        Container(
                          margin: EdgeInsets.only(top: 29, bottom: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 2, right: 2),
                                child: GestureDetector(
                                  onTap: () {},
                                  child: new Text(
                                    "I don't have an account.",
                                    style: TextStyle(fontSize: 14.0, fontFamily: 'poppins_regular', color: Color(0xff4B4B4B)),
                                    textAlign: TextAlign.end,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 7),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2, right: 5, bottom: 2),
                                  child: GestureDetector(
                                    child: new Text(
                                      "Sign up",
                                      style: TextStyle(decoration: TextDecoration.underline, fontSize: 14.0, fontFamily: 'poppins_regular', color: Color(0xff52A811)),
                                      textAlign: TextAlign.end,
                                    ),
                                    onTap: () {
                                      phoneNumberController.text = "";
                                      passwordController.text = "";
                                      Navigator.pushNamed(context, '/signup_phone');
                                      /*Navigator.push(
                                            context,
                                            CupertinoPageRoute(
                                              builder: (context) =>
                                                  RegistrationStep1Screen(),
                                            ));*/
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
