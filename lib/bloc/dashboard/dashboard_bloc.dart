import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_revision/model/login_response.dart';
import 'package:flutter_revision/repository/dashboard/dashboard_repository.dart';
import 'package:flutter_revision/util/AlertMessages.dart';

import 'dashboard_events.dart';
import 'dashboard_states.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  late DashboardRepository _dashboardRepository;
  late UserData userData;

  DashboardBloc() : super(InitialState());

  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    _dashboardRepository = DashboardRepository();
    try {
      if (event is GetDashboardData) {
        // Outputting a state from the asynchronous generator
        yield LoadingState();
        final apiResponse = await _dashboardRepository.getDashboardData(event.userDealerRelId);
        UserData? data = apiResponse.data;
        if (apiResponse.code == 200 && data != null) {
          userData = data;
          yield SuccessState(dashboardData: data);
        } else {
          String? message = apiResponse.message;
          yield ErrorState(errorMessage: message != null ? message : AlertMessages.GENERIC_ERROR_MSG);
        }
      }
    } catch (_) {
      print(_);
      yield ErrorState(errorMessage: AlertMessages.GENERIC_ERROR_MSG);
    }
  }
}
