import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_revision/bloc/user/user_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'dashboard_screen.dart';
import 'others_screen.dart';

class HomeTabsScreen extends StatefulWidget {
  HomeTabsScreen({Key? key}) : super(key: key);

  @override
  HomeTabsScreenState createState() {
    return new HomeTabsScreenState();
  }
}

class HomeTabsScreenState extends State<HomeTabsScreen> {
  final homeTabsGlobalKey = GlobalKey<ScaffoldState>();
  Color selectedColor = Colors.grey;
  ValueNotifier<int> _selectedTabIndex = ValueNotifier(0);
  ValueNotifier<int> _overlayIndex = ValueNotifier(0);
  ValueNotifier<bool> _showDashboardTutorial = ValueNotifier(false);
  late UserBloc userBloc;

  @override
  void initState() {
    super.initState();
    /*  _selectedTabIndex.addListener(() {

    });*/
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onTabTapped(int index) {
    _selectedTabIndex.value = index;
  }

  void _onTabTapped(int index) {
    _selectedTabIndex.value = index;
  }

  Future<bool> _onBackPressed() {
    if (_selectedTabIndex.value != 0) {
      _selectedTabIndex.value = 0;
      return Future<bool>.value(false);
    } else {
      return Future<bool>.value(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    userBloc = Provider.of<UserBloc>(context);

    final List<Widget> _children = [
      DashboardScreen(
        onscreenLoaded: (bool) => {},
        onTabSelected: (int) => {},
      ),
      OthersScreen(),
      OthersScreen(),
      OthersScreen(),
    ];

    return ValueListenableBuilder(
      valueListenable: _selectedTabIndex,
      builder: (context, currentSelectedIndex, _) {
        return WillPopScope(
          onWillPop: _onBackPressed,
          child: Stack(
            children: <Widget>[
              Scaffold(
                key: homeTabsGlobalKey,
                body: _children[currentSelectedIndex as int],
                bottomNavigationBar: Theme(
                  data: Theme.of(context).copyWith(
                    // sets the background color of the `BottomNavigationBar`
                    canvasColor: Colors.white,
                    // sets the active color of the `BottomNavigationBar` if `Brightness` is light

                    primaryColor: Color(0xff52A811),
                    textSelectionColor: Color(0xff52A811),
                    disabledColor: Color(0xff959595),
                    textTheme: Theme.of(context).textTheme.copyWith(
                          caption: TextStyle(fontSize: 14, fontFamily: 'poppins_regular', color: const Color(0xff959595)),
                        ),
                  ),
                  child: BottomNavigationBar(
                    backgroundColor: Colors.white,
                    /*fixedColor: Color(0xff959595),
            selectedItemColor: Color(0xff52A811),
            unselectedItemColor: Color(0xff959595),*/
                    type: BottomNavigationBarType.fixed,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        backgroundColor: Colors.blue,
                        activeIcon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/dashboard_active.svg",
                            color: Color(0xff52A811),
                            width: 20,
                            height: 18,
                          ),
                        ),
                        icon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/dashboard_active.svg",
                            color: Color(0xff959595),
                            width: 20,
                            height: 18,
                          ),
                        ),
                        title: Text("Home",
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'poppins_regular',
                            )),
                      ),
                      BottomNavigationBarItem(
                        activeIcon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/tab_my_sales_active.svg",
                            width: 20,
                            height: 18,
                          ),
                        ),
                        icon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/tab_my_sales.svg",
                            width: 20,
                            height: 18,
                          ),
                        ),
                        title: Text("Performance",
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'poppins_regular',
                            )),
                      ),
                      BottomNavigationBarItem(
                        activeIcon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/district_sales_active.svg",
                            width: 20,
                            height: 18,
                          ),
                        ),
                        icon: Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: SvgPicture.asset(
                            "assets/icons/district_sales.svg",
                            width: 20,
                            height: 18,
                          ),
                        ),
                        title: Text(
                          "Report",
                          style: TextStyle(fontSize: 14, fontFamily: 'poppins_regular'),
                        ),
                      ),
                    ],
                    onTap: _onTabTapped,
                    currentIndex: currentSelectedIndex,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
