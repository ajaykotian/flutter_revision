import 'package:flutter/material.dart';

abstract class DashboardEvent {}

class GetDashboardData extends DashboardEvent {
  final String userDealerRelId;

  GetDashboardData({required this.userDealerRelId});
}
