import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_revision/constants/app_colors.dart';
import 'package:flutter_revision/util/AlertMessages.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AlertUtil {
  static void showSnackBar(BuildContext context, String meesage) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(meesage),
    ));
  }

  static void showToast(BuildContext context, String meesage) {
    Fluttertoast.showToast(msg: meesage, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, backgroundColor: Colors.black38, textColor: Colors.white, fontSize: 16.0);
  }

  static void showSnackBarWithKey(GlobalKey<ScaffoldState> _scaffoldKey, String meesage) {
    _scaffoldKey.currentState?.showSnackBar(new SnackBar(content: new Text(meesage)));
  }

  static void showAlertPopup(BuildContext context, String title, String? msg) {
    String nullSafeMsg = AlertMessages.GENERIC_ERROR_MSG;
    if (msg != null) {
      nullSafeMsg = msg;
    }

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: FittedBox(
              child: Container(
                width: MediaQuery.of(context).size.width / 1.3,
                padding: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  /* boxShadow: [
                    BoxShadow(
                      color: Color(0xff14000000),
                      blurRadius: 6.0,
                      offset: Offset(0, 3),
                    ),
                  ],*/
                ),
                child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisAlignment: MainAxisAlignment.center, children: [
                  Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: TextStyle(decoration: TextDecoration.none, fontSize: 18, fontFamily: 'poppins_medium', color: AppColors.primaryColor),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20, bottom: 20, left: 5, right: 5),
                    child: Text(
                      nullSafeMsg,
                      textAlign: TextAlign.center,
                      style: TextStyle(decoration: TextDecoration.none, fontSize: 14, height: 1.5, fontFamily: 'poppins_regular', color: Colors.black54),
                    ),
                  ),
                  Divider(
                    height: 0.3,
                    color: Color(0xff905C5C5C),
                  ),
                  Container(
                    child: FlatButton(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('OK', style: TextStyle(decoration: TextDecoration.none, fontSize: 17, fontFamily: 'Metropolis_Biold', color: Colors.black54)),
                    ),
                  ),
                ]),
              ),
            ),
          );
        });
  }
}
