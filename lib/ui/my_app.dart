import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_revision/constants/app_constants.dart';
import 'package:flutter_revision/ui/splash_screen.dart';
import 'package:flutter_revision/util/Routes.dart';

import 'home/home_tabs_screen.dart';
import 'login/login_screen.dart';

class MyApp extends StatelessWidget {
  static final myTabbedPageKey = new GlobalKey<HomeTabsScreenState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppConstants.themeData,
      home: SplashScreen(),
      key: myTabbedPageKey,
      routes: {
        Routes.loginRoute: (context) => LoginScreen(),
        Routes.homeRoute: (context) => HomeTabsScreen(key: key),
      },
    );
  }
}
