class AlertMessages {
  static const String GENERIC_ERROR_MSG =
      "Something went wrong. Please try again.";
  static const String TIMEOUT_ERROR_MSG =
      "Failed to connect to server. Please try again.";
  static const String OFFLINE_ERROR_MSG =
      "Please check your internet connection and try again.";
  static const String WRONG_LOGIN_CREDENTIALS =
      "Wrong mobile number or password. Please try again.";
}