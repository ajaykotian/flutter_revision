import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_revision/ui/my_app.dart';
import 'package:provider/provider.dart';

import 'bloc/user/user_bloc.dart';
import 'constants/app_colors.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: AppColors.darkPrimaryColor,
    statusBarBrightness: Brightness.light,
  ));

  runZoned(
    () {
      runApp(
        ChangeNotifierProvider<UserBloc>(
          create: (context) => UserBloc(),
          child: MyApp(),
        ),
      );
    },
  );
}
