import 'package:flutter/material.dart';

class AppColors {
  static const Map<int, Color> color = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };

  static const MaterialColor primaryColor = MaterialColor(0xff67BB27, color);
  static const MaterialColor darkPrimaryColor = MaterialColor(0xff3A7D08, color);
  static const MaterialColor semiPrimary = MaterialColor(0xff67BB27, color);
  static const MaterialColor accentColor = MaterialColor(0xff67BB27, color);
  static const MaterialColor screenBgColor = MaterialColor(0xffFCFCFC, color);
}
