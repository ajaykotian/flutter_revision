import 'package:flutter_revision/model/dashboard_response.dart';
import 'dashboard_api_provider.dart';

class DashboardRepository {
  DashboardApiProvider _dashboardApiProvider = DashboardApiProvider();

  Future<DashboardResponse> getDashboardData(String userDealerRelId) => _dashboardApiProvider.getDashboardData(userDealerRelId);
}
