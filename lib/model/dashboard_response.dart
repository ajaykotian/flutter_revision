import 'package:flutter_revision/model/login_response.dart';

class DashboardResponse {
  int? code;
  String? message;
  UserData? data;

  DashboardResponse({this.code, this.message, this.data});

  DashboardResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null ? new UserData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data?.toJson();
    }
    return data;
  }
}
